import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { AppLoading } from "expo";
import { useFonts } from 'expo-font';

import MyApp from './src' ;

const customFonts = {
  MontserratLight: require("./assets/fonts/Montserrat-Light.ttf"),
  MontserratBold: require("./assets/fonts/Montserrat-Bold.ttf"),
  MontserratSemiBold: require("./assets/fonts/Montserrat-SemiBold.ttf"),
  MontserratBlack: require("./assets/fonts/Montserrat-Black.ttf"),
  MontserratMedium: require("./assets/fonts/Montserrat-Medium.ttf"),
  Montserrat: require("./assets/fonts/Montserrat-Regular.ttf"),
};

export default function App() {
  const [isLoaded] = useFonts(customFonts);


  if (!isLoaded) {
      return <AppLoading />;
  }

  return (
    <MyApp/>
  );
}