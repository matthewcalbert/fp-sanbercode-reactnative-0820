import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity,
    TextInput,
} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import styles from './styles';

const FormControl = (props)=>{
    return(
        <View style={{width:'100%',paddingVertical:10}}>
            <Text style={styles.formLabel}>{props.name}</Text>
            <TextInput style={styles.inputText} placeholder={props.placeholder}/>
            {props.children}
        </View>
    )
}

export default ({navigation})=>{
    return (
        <View style={styles.container}>
            <View style={{width:"100%",flexDirection:"row",justifyContent:"space-evenly"}}>
                <TouchableOpacity style={styles.mainNavLink} onPress={()=>{navigation.navigate('HomeScreen')}}>
                    <Icon name="home" color="white" size={18} style={{marginRight:5}}/>
                    <Text style={styles.navLinkText}>Home</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.mainNavLink} onPress={()=>{navigation.navigate('AboutScreen')}}>
                    <Icon name="information" color="white" size={18} style={{marginRight:5}}/>
                    <Text style={styles.navLinkText}>About</Text>
                </TouchableOpacity>
            </View>
            <View style={styles.body}>
                <View style={{width:"100%", marginTop:30}}>
                    <Text style={styles.formTitle}>Login</Text>
                    <Text style={styles.formDesc}>Sign in to continue</Text>
                </View>
                <View style={{width:"100%", marginTop: 50}}>
                    <FormControl name="Email" placeholder="budi@example.com" />
                    <FormControl name="Password" placeholder="**********" />
                    <TouchableOpacity><Text style={{...styles.hlink, textAlign:"right"}}>forgot password ?</Text></TouchableOpacity>
                        
                        <View style={{marginTop:50}}/>
                    <TouchableOpacity style={styles.btn}>
                        <Text style={styles.btnTxt}>Login</Text>
                    </TouchableOpacity>
                    <Text style={{fontFamily:"MontserratBold", fontSize:14, textAlign:"center", paddingVertical: 10}}>OR</Text>
                    <TouchableOpacity style={styles.btn} onPress={()=>{navigation.navigate('RegisterScreen')}}>
                        <Text style={styles.btnTxt}>Register</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    )
}