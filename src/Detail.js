import React, {useState, useContext} from "react";
import axios from 'axios';
import { StyleSheet, Text, View, Image, TouchableOpacity, ScrollView } from "react-native";

import styles from './styles';

import { GlobalContext } from './GlobalContext';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

export default ({route, navigation}) => {
    const [provinceData, setProvinceData] = useContext(GlobalContext);
    const [currentData, setCurrentData] = useState(null);
    const {Kode_Provi} = route.params;

    const getData = ()=>{
        if( currentData === null ) return <Text>IsLoading</Text>
        if( currentData === false ) return <Text>Invalid</Text>
        let item = currentData;
        return (
            <>
            <Text style={styles.dataText}>{item.attributes.Kasus_Posi}</Text>
            <Text style={styles.dataText}>{item.attributes.Kasus_Semb}</Text>
            <Text style={styles.dataText}>{item.attributes.Kasus_Meni}</Text>
            <Text style={styles.dataText}>{item.attributes.Kasus_Meni}</Text>
            </>
        )
    }

    const getTitle = ()=>{
        if( currentData === null ) return <Text>IsLoading</Text>
        if( currentData === false ) return <Text>Invalid</Text>
        let item = currentData;
        return item.attributes.Provinsi;
    }

    const findProvince = (rdata) => {
        for( let i = 0 ; i < rdata.length ; i++ ){
            let item = rdata[i];
            if( item.attributes.Kode_Provi == Kode_Provi ){
                setCurrentData(item);
                return;
            }
        }
        setCurrentData(false);
    }

    const fetchData = ()=>{
        axios.get(`https://api.kawalcorona.com/indonesia/provinsi/`)
        .then(res => {
            let rdata = res.data;
            setProvinceData(rdata);
        });
    }

    if( currentData === null ){
        if( provinceData !== null  ) findProvince(provinceData);
        else fetchData();
    }

    return (
      <View style={styles.container}>
          <View style={{width:"100%",flexDirection:"row",justifyContent:"space-evenly"}}>
                <TouchableOpacity style={styles.mainNavLink} onPress={()=>{navigation.navigate('HomeScreen')}}>
                    <Icon name="home" color="white" size={18} style={{marginRight:5}}/>
                    <Text style={styles.navLinkText}>Home</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.mainNavLink} onPress={()=>{navigation.navigate('AboutScreen')}}>
                    <Icon name="information" color="white" size={18} style={{marginRight:5}}/>
                    <Text style={styles.navLinkText}>About</Text>
                </TouchableOpacity>
          </View>
          <View style={styles.body}>
                <View style={{width:"100%"}}>
                <TouchableOpacity style={{flexDirection:"row", alignItems:"center"}}onPress={()=>{navigation.goBack()}}>
                    <Icon name="arrow-left" size={22} style={{marginRight:7}}/>
                    <Text style={{...styles.subTitle, fontSize:14}}>Back</Text>
                </TouchableOpacity>
                <View style={{marginTop:30}}/>
                <Text style={styles.title}>Data Covid</Text>
                <Text style={styles.subTitle}>{getTitle()}</Text>
                </View>
                <View style={styles.mainData}>
                    <View>
                        <Text style={styles.dataText}>Positif</Text>
                        <Text style={styles.dataText}>Sembuh</Text>
                        <Text style={styles.dataText}>Meninggal</Text>
                        <Text style={styles.dataText}>Dirawat</Text>
                    </View>
                    <View>
                        {getData()}
                    </View>
                </View>
          </View>
      </View>
    )

}