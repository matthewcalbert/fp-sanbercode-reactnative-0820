import React from "react";
import { StyleSheet, Image, Text, View } from "react-native";

import logo from './assets/logo-2.png';

export default Splash = ()=>{
    return (
        <View style={styles.container}>
            <View style={styles.roundContainer}>
                <Image source={logo} style={styles.logoImage}/>
                <Text style={{fontSize:24, fontFamily:"MontserratBold"}}>CoronaMarket</Text>
            </View>
            <Text style={{fontSize:12, fontFamily:"MontserratSemiBold", position:"absolute", bottom: 40}}>SanberFinal @ 2020</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        height: "100%",
        paddingHorizontal: 10,
        justifyContent: "center",
        backgroundColor: "white",
        alignItems: 'center',
    },
    roundContainer:{
        alignItems: "center",
        alignContent: "center",
        justifyContent: "center"
    },
    logoImage:{
        width: 200,
        height: 200,    
    }
})