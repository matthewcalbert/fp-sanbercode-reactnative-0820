import { StyleSheet } from "react-native";


export default StyleSheet.create({
    container:{
        flex: 1,
        height: "100%",
        width: "100%",
        paddingTop: 50,
        backgroundColor: "#8A479B"
    },
    body:{
        marginTop: 20,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        alignItems: "center",
        paddingVertical: 20,
        paddingHorizontal: 40,
        paddingBottom: "20%",
        width: "100%",
        backgroundColor: "white",
        height:"100%"
    },
    bodyAlt:{
        marginTop: 20,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        paddingVertical: 20,
        paddingHorizontal: 40,
        paddingBottom: "20%",
        width: "100%",
        backgroundColor: "white",
        height:"100%"
    },
    mainNavLink:{
        flexDirection:"row",
        alignItems: "center",
        borderRadius: 10,
        paddingHorizontal: 20,
        paddingVertical: 5,
    },
    navLinkText:{
        fontFamily:"MontserratBold",
        fontSize: 14,
        color: "white",
    },
    card:{
        width: "100%",
        backgroundColor: "red",
        borderRadius: 5,
    },
    cardTitle:{
        fontFamily:"MontserratSemiBold",
        fontSize: 18,
        color: "white"
    },
    smBtn:{
        color: "white",
        width: 140,
        textAlign: "center",
        borderRadius: 5,
        fontSize: 14,
        fontFamily: "MontserratBold",
        padding: 7,
        backgroundColor: "#8A479B",
    },
    title:{
        fontFamily: "MontserratBold",
        fontSize: 18,
        marginRight: 10,
    },
    subTitle:{
        fontFamily: "MontserratMedium",
        fontSize: 18,
    },
    mainData:{
        flexDirection: "row",
        marginVertical: 20,
        width: "100%",
        padding: 20,
        borderRadius: 5,
        backgroundColor: "#DE6278",
        justifyContent: "space-between"
    },
    dataText:{
        fontFamily: "MontserratBold",
        fontSize: 14,
        color: "white",
    },
    list:{
        width: "100%",
        borderRadius: 5,
        flexDirection: "row",
        paddingVertical: 15,
        paddingHorizontal: 20,
        justifyContent:"space-between",
        backgroundColor: "#ED4563",
        marginBottom: 10,
    },
    ltext:{
        fontFamily: "MontserratMedium",
        fontSize: 14,
        textAlign: "left",
    },
    formTitle:{
        fontFamily: "MontserratBold",
        fontSize: 24,
    },
    formDesc:{
        fontFamily: "MontserratMedium",
        fontSize: 14,
    },
    formLabel:{
        fontFamily: "MontserratBold",
        fontSize: 18,
    },
    inputText:{
        fontFamily: "MontserratMedium",
        fontSize: 14,
        paddingVertical: 7,
        borderBottomWidth: 1,
        borderColor: "#6C6C6C"
    },
    hlink:{
        fontFamily: "MontserratBold",
        fontSize: 13,
        color: "#8A479B",
    },
    btn:{
        backgroundColor: "#DE6278",
        width: "100%",
        alignItems: "center",
        padding: 10,
        borderRadius: 5,
    },
    btnTxt:{
        fontFamily: "MontserratBold",
        fontSize: 14,
        color: "white"
    }
})