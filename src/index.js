import React, { useEffect, useState } from "react";
import { StyleSheet, Text, View } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

import { GlobalProvider } from './GlobalContext';

import Splash from './Splash';
import Login from './Login';
import Register from './Register';
import Home from './Home';
import About from './About';
import Detail from './Detail';

const RootStack = createStackNavigator();

const RootStackScreen = () => {
    return (
      <GlobalProvider>
      <RootStack.Navigator screenOptions={{
        headerShown: false
      }}>
          <RootStack.Screen name="HomeScreen" component={Home}/>
          <RootStack.Screen name="DetailScreen" component={Detail}/>
          <RootStack.Screen name="RegisterScreen" component={Register}/>
          <RootStack.Screen name="LoginScreen" component={Login}/>
          <RootStack.Screen name="AboutScreen" component={About}/>
      </RootStack.Navigator>
      </GlobalProvider>
    )

}

export default () => {
    const [isLoading, setIsLoading] = useState(true);


    useEffect(()=>{
        setTimeout(()=>{
          if( isLoading ) setIsLoading(false);
      },2000);
    })

    if( isLoading ) return <Splash/>;
    return (
      <NavigationContainer>
          <RootStackScreen/>
      </NavigationContainer>
    );
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        height: "100%",
        paddingHorizontal: 10,
        justifyContent: "center",
        backgroundColor: "white",
        alignItems: 'center',
    },
})