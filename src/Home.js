import React, { useEffect, useState, useContext } from "react";
import { StyleSheet, Text, View, Image, TouchableOpacity, ScrollView, SafeAreaView } from "react-native";
import axios from 'axios';
import { AppLoading } from "expo";

import { GlobalContext } from './GlobalContext';

import styles from './styles';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import inaFlag from './assets/indonesia-flag.png'

export default ({navigation}) => {
    const [isLoading, setIsLoading] = useState(true);
    const [indonesiaData, setIndonesiaData] = useState(null);
    const [provinceData, setProvinceData] = useContext(GlobalContext);

    const fetchData = ()=>{
        axios.get(`https://api.kawalcorona.com/indonesia/`)
        .then(res => {
            let rdata = res.data;
            setIndonesiaData(rdata[0])
            
        });

        axios.get(`https://api.kawalcorona.com/indonesia/provinsi/`)
        .then(res => {
            let rdata = res.data;
            setProvinceData(rdata)
        });
    }

    useEffect(()=>{
        if( indonesiaData != null && provinceData != null ){
            if ( isLoading ) setIsLoading(false);
        }
    })

    if( indonesiaData == null && provinceData == null ){
        fetchData();
    }

    const renderItem = (item,id) => {
      return (
        <TouchableOpacity style={styles.list} key={id} onPress={()=>{navigation.navigate('DetailScreen',{Kode_Provi:item.attributes.Kode_Provi})}}>
            <Text style={styles.dataText}>{item.attributes.Provinsi}</Text>
            <Icon name="chevron-right" size={20} color="white"/>
        </TouchableOpacity>
      )
    }

    const getIndoData = ()=>{
        if( indonesiaData === null ) return <Text>isLoading</Text>
        return (
            <>
            <Text style={styles.dataText}>{indonesiaData.positif}</Text>
            <Text style={styles.dataText}>{indonesiaData.sembuh}</Text>
            <Text style={styles.dataText}>{indonesiaData.meninggal}</Text>
            <Text style={styles.dataText}>{indonesiaData.dirawat}</Text>
            </>
        )
    }

    return (
      <SafeAreaView style={styles.container}>
          <View style={{width:"100%",flexDirection:"row",justifyContent:"space-evenly"}}>
            <View style={{...styles.mainNavLink, backgroundColor: "#CA99C6"}}>
                <Icon name="home" color="white" size={18} style={{marginRight:5}}/>
                <Text style={styles.navLinkText}>Home</Text>
            </View>
            <TouchableOpacity style={styles.mainNavLink} onPress={()=>{navigation.navigate('AboutScreen')}}>
                <Icon name="information" color="white" size={18} style={{marginRight:5}}/>
                <Text style={styles.navLinkText}>About</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.body}>
              <View style={{width:"100%",flexDirection:"row",justifyContent:"space-between"}}>
                <Text style={styles.smBtn} onPress={()=>{navigation.navigate('LoginScreen')}}>Login</Text>
                <Text style={styles.smBtn} onPress={()=>{navigation.navigate('RegisterScreen')}}>Register</Text>
              </View>
              <View style={{flexDirection:"row", alignItems: "center", width:"100%", marginTop:20,}}>
                <Text style={styles.title}>Data Covid Indonesia</Text>
                <Image source={inaFlag} style={{height: 17, width:26, borderWidth: 1, borderColor:"black"}}/>
              </View>
              <View style={styles.mainData}>
                <View>
                    <Text style={styles.dataText}>Positif</Text>
                    <Text style={styles.dataText}>Sembuh</Text>
                    <Text style={styles.dataText}>Meninggal</Text>
                    <Text style={styles.dataText}>Dirawat</Text>
                </View>
                <View>
                    {getIndoData()}
                </View>
              </View>
              <View style={{flexDirection:"row", alignItems: "center", width:"100%",}}>
                <Text style={styles.title}>Daftar Provinsi</Text>
              </View>
              <ScrollView style={{width:"100%",marginTop:20}}>
                {
                    provinceData && provinceData.map(renderItem)
                }
              </ScrollView>
          </View>
      </SafeAreaView>
    )

}