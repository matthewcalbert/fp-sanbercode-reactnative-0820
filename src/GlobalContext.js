import React, { useState, createContext } from 'react';

export const GlobalContext = createContext();

export const GlobalProvider = props =>{
    const [currentProvince,setProvince] = useState(null);

    return (
        <GlobalContext.Provider value={[currentProvince,setProvince]}>
            {props.children}
        </GlobalContext.Provider>
    )
}