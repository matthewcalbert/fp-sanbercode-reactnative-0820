import React from "react";
import { StyleSheet, Text, View, Image, TouchableOpacity, ScrollView } from "react-native";

import styles from './styles';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import logo from './assets/logo-2.png';

export default ({navigation}) => {
    return (
      <View style={styles.container}>
          <View style={{width:"100%",flexDirection:"row",justifyContent:"space-evenly"}}>
            <TouchableOpacity style={styles.mainNavLink} onPress={()=>{navigation.navigate('HomeScreen')}}>
                <Icon name="home" color="white" size={18} style={{marginRight:5}}/>
                <Text style={styles.navLinkText}>Home</Text>
            </TouchableOpacity>
            <View style={{...styles.mainNavLink, backgroundColor: "#CA99C6"}}>
                <Icon name="information" color="white" size={18} style={{marginRight:5}}/>
                <Text style={styles.navLinkText}>About</Text>
            </View>
          </View>
          <View style={styles.body}>
                <View style={{}}>
                    <Image source={logo} style={{width:150,height:150}}/>
                    <Text style={{fontSize:18, fontFamily:"MontserratBold"}}>CoronaMarket</Text>
                </View>
                <View style={{flexDirection:"row", alignItems: "center", width:"100%",marginTop: 50}}>
                    <Text style={styles.title}>About</Text>
                </View>
                <View style={{alignItems:"center",width:"100%",marginTop:20}}>
                    <Text style={styles.ltext}>Proyek Akhir Sanbercode React Native.{"\n"}
                    Aplikasi ini berisi tentang contoh implementasi API corona pada aplikasi mobile.{"\n\n"}
                    Menggunakan API: 
                    </Text>
                    <Text style={{...styles.ltext, fontFamily:"MontserratSemiBold", alignSelf:"flex-start"}}>api.kawalcorona.com</Text>
                </View>
                <View style={{flexDirection:"row", alignItems: "center", width:"100%",marginTop:30}}>
                <Text style={styles.title}>Data Peserta</Text>
                </View>
                <View style={{alignItems:"center",width:"100%",marginTop:20}}>
                    <Text style={{...styles.ltext, alignSelf:"flex-start"}}>
                    Matthew Christopher Albert{"\n"}
                    matthewmcavaio@gmail.com
                    </Text>
                </View>
          </View>
      </View>
    )

}